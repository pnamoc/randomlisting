# RandomListing

RandomListing is just a test project that showcase simple MVVM pattern with using tableview with rxswift programmatically.

## Functionality

```bash
- Display List of Products
- Display Product Details with the use of SKU.
- Refresh Button
- Sorting Button
- Bookmark product details
```

## Libraries Used

- Snapkit - used for constraints.
- Alamofire - for network call.
- AlamofireNetworkActivityLogger - for json api logger.
- SwiftyUserDefaults - for token saving.
- NVActivityIndicatorView - for activity indicator.
- Realm - for local database.
