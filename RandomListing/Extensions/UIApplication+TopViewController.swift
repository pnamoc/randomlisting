//
//  UIApplication+TopViewController.swift
//  RandomListing
//
//  Created by Louise Nicolas Namoc on 6/18/21.
//

import Foundation
import UIKit

extension UIWindow {
  static var key: UIWindow? {
    if #available(iOS 13, *) {
      return UIApplication.shared.windows.first { $0.isKeyWindow }
    } else {
      return UIApplication.shared.keyWindow
    }
  }
}

extension UIApplication {
  class func topViewController(
    controller: UIViewController? = UIWindow.key?.rootViewController
  ) -> UIViewController? {
    if let navigationController = controller as? UINavigationController {
      return topViewController(controller: navigationController.visibleViewController)
    }
    if let tabController = controller as? UITabBarController {
      if let selected = tabController.selectedViewController {
        return topViewController(controller: selected)
      }
    }
    if let presented = controller?.presentedViewController {
      return topViewController(controller: presented)
    }
    return controller
  }
}
