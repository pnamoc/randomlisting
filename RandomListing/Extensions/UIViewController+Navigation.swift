//
//  UIViewController+Navigation.swift
//  RandomListing
//
//  Created by Louise Nicolas Namoc on 6/21/21.
//

import UIKit

extension UIViewController {
  func addRightBarButtonItem(
    with image: UIImage,
    target: Any?,
    selector: Selector
  ) {
    navigationItem.addRightBarButtonItem(with: image, target: target, selector: selector)
  }
}
