//
//  UIButton+Color.swift
//  RandomListing
//
//  Created by Louise Nicolas Namoc on 6/18/21.
//

import Foundation
import UIKit

extension UIButton {
  func updateTint(color: UIColor?) {
    let imageButton = imageView?.image?.withRenderingMode(.alwaysTemplate)
    setImage(imageButton, for: .normal)
    tintColor = color
  }
}
