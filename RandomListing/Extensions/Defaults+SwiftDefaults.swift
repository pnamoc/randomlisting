//
//  Defaults+SwiftDefaults.swift
//  RandomListing
//
//  Created by Louise Nicolas Namoc on 6/18/21.
//

import Foundation
import SwiftyUserDefaults

extension DefaultsKeys {
  var token: DefaultsKey<String?> { .init("token", defaultValue: "") }
}
