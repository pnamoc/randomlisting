//
//  UINavigationItem.swift
//  RandomListing
//
//  Created by Louise Nicolas Namoc on 6/21/21.
//

import UIKit

extension UINavigationItem {
  func addRightBarButtonItem(
    with image: UIImage,
    target: Any?,
    selector: Selector
  ) {
    let rightBarButton = UIButton(type: .custom)
    rightBarButton.setImage(image, for: .normal)
    rightBarButton.frame = CGRect(x: 0, y: 0, width: 32, height: 32)
    rightBarButton.addTarget(target, action: selector, for: .touchUpInside)

    let rightBarItem = UIBarButtonItem(customView: rightBarButton)
    rightBarButtonItem = rightBarItem
  }
}
