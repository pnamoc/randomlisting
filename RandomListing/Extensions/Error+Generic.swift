//
//  Error+Generic.swift
//  RandomListing
//
//  Created by Louise Nicolas Namoc on 6/18/21.
//

import Foundation

extension NSError {
  static func generic(message: String, code: Int? = -1) -> NSError {
    return NSError(domain: "Error", code: code!, userInfo: ["message": message])
  }
}
