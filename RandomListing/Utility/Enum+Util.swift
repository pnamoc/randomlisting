//
//  Enum+Util.swift
//  RandomListing
//
//  Created by Louise Nicolas Namoc on 6/18/21.
//

import Foundation

enum RestVerbs {
  case POST
  case GET
  case PUT
  case DELETE
  case PATCH
}
