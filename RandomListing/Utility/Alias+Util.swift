//
//  Alias+Util.swift
//  RandomListing
//
//  Created by Louise Nicolas Namoc on 6/18/21.
//

import Foundation


typealias ResultSuccess<T> = ((T?, Bool, String?) -> Void)
typealias OnCompletionHandler<T> = ((T?, Error?) -> Void)
typealias SingleResultWithReturn<T, ReturnType> = ((T) -> ReturnType)
typealias SingleResult<T> = SingleResultWithReturn<T, Void>
typealias DoubleResultWithReturn<T1, T2, ReturnType> = ((T1, T2) -> ReturnType)
typealias DoubleResult<T1, T2> = DoubleResultWithReturn<T1, T2, Void>
typealias Param = [String: Any]
typealias BoolResult = SingleResult<Bool> // (Bool) -> Void

