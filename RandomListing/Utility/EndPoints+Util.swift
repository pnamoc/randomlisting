//
//  EndPoints+Util.swift
//  RandomListing
//
//  Created by Louise Nicolas Namoc on 6/18/21.
//

import Foundation

let apiVersion = "V1"
let baseUri = "https://milwaukee.dtndev.com/rest/default/\(apiVersion)"


let tokenEndPoint = "/integration/admin/token"
let listProductEndPoint = "/products"
