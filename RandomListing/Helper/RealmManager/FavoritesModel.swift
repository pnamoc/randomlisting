//
//  Favorites.swift
//  RandomListing
//
//  Created by Louise Nicolas Namoc on 6/21/21.
//

import RealmSwift
import Foundation

@objcMembers class FavoritesModel: Object {
  @objc dynamic var name: String = ""
  @objc dynamic var sku: String = ""

}
