//
//  BaseService.swift
//  RandomListing
//
//  Created by Louise Nicolas Namoc on 6/18/21.
//

import Foundation

import Alamofire
import SwiftyUserDefaults

class BaseService {
  func consumeToken(
    usingUrl url: String,
    withVerb verb: RestVerbs,
    andParameters params: Parameters? = nil,
    completion: DoubleResult<String, Error?>? = nil
  ) {
    let apiURL: String = baseUri + url

    AF.request(
      apiURL,
      method: getVerb(type: verb),
      parameters: params,
      encoding: JSONEncoding.default
    ).responseJSON { response in
      switch response.result {
      case .success:
        let token = response.value as? String
        Defaults[\.token] = token
        completion?(token ?? "", nil)
      case let .failure(error):
        completion?("", error)
      }
    }
  }

  func consumeAPI<T: Decodable>(
    _ decodableType: T.Type,
    usingUrl url: String,
    withVerb verb: RestVerbs,
    andParameters params: Parameters? = nil,
    completion: OnCompletionHandler<T>? = nil
  ) {
    var apiURL: String = baseUri + url
    apiURL = apiURL.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) ?? ""

    var httpHeader: HTTPHeaders?
    if let validToken = Defaults[\.token], !validToken.isEmpty {
      httpHeader = ["Authorization": "Bearer \(validToken)"]
    }

    AF.request(
      apiURL,
      method: getVerb(type: verb),
      parameters: params,
      encoding: URLEncoding.default,
      headers: httpHeader
    ).responseJSON { response in

      switch response.result {
      case .success:
        if let data = response.data {
          let decoder = JSONDecoder()
          let statusCode = response.response?.statusCode
          if statusCode! >= 200 && statusCode! <= 299 {
            let value = try! decoder.decode(T.self, from: data)
            completion?(value, nil)
          } else {
            let value = try! JSONDecoder().decode(T.self, from: data)
            completion?(
              value,
              NSError.generic(
                message: "Value is not \(String(describing: T.self))",
                code: response.response?.statusCode
              )
            )
          }
        } else {
          completion?(
            nil,
            NSError.generic(
              message: "Value is not \(String(describing: T.self))",
              code: -1
            )
          )
        }
      case let .failure(error):
        completion?(nil, error as NSError)
      }
    }
  }

  private func getVerb(type: RestVerbs) -> HTTPMethod {
    var localVerb: HTTPMethod!
    switch type {
    case .POST:
      localVerb = .post
    case .GET:
      localVerb = .get
    case .PUT:
      localVerb = .put
    case .DELETE:
      localVerb = .delete
    case .PATCH:
      localVerb = .patch
    }
    return localVerb
  }
}
