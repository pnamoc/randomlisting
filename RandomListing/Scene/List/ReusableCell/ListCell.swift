//
//  ListCell.swift
//  RandomListing
//
//  Created by Louise Nicolas Namoc on 6/18/21.
//

import UIKit

class ListCell: UITableViewCell {
  static let identifier = "CellListIdentifier"

  // Sku Label Creation
  private let skuLabel: UILabel = {
    let label = UILabel()
    label.textColor = .black
    label.font = UIFont.systemFont(ofSize: 16)
    label.textAlignment = .left
    label.numberOfLines = 0
    return label
  }()

  // Name Label Creation
  private let nameLabel: UILabel = {
    let label = UILabel()
    label.textColor = .black
    label.font = UIFont.systemFont(ofSize: 16)
    label.textAlignment = .left
    label.numberOfLines = 0

    return label
  }()

  // Thinline View Creation
  private let thinLine: UIImageView = {
    let imageView = UIImageView()
    imageView.backgroundColor = .lightGray
    return imageView
  }()

  override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
    super.init(style: style, reuseIdentifier: reuseIdentifier)
    initSubviews()
  }

  private func initSubviews() {
    backgroundColor = .clear
    selectionStyle = .none

    let stackView = UIStackView()
    stackView.axis = .vertical
    stackView.distribution = .fill
    stackView.alignment = .fill
    stackView.spacing = 0.0

    stackView.addArrangedSubview(skuLabel)
    stackView.addArrangedSubview(nameLabel)

    contentView.addSubview(stackView)
    contentView.addSubview(thinLine)

    stackView.snp.makeConstraints { m in
      m.leadingMargin.equalTo(5)
      m.trailingMargin.equalTo(-5)
      m.topMargin.equalTo(5)
      m.bottomMargin.equalTo(5)
    }

    thinLine.snp.makeConstraints { m in
      m.height.equalTo(1)
      m.leading.trailing.equalToSuperview()
      m.bottom.equalToSuperview().offset(2)
    }
  }

  required init?(coder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }

  func configureCell(item: Item) {
    skuLabel.text = item.sku
    nameLabel.text = item.name
  }
}
