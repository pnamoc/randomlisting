//
//  ListViewModel.swift
//  RandomListing
//
//  Created by Louise Nicolas Namoc on 6/18/21.
//

import Foundation
import RxCocoa
import RxSwift

class ListViewModel {
  private var service: ListServicesProtocol
  var items = BehaviorRelay<[Item]>(value: [])

  init(service: ListServicesProtocol) {
    self.service = service
  }

  func getToken(completion: @escaping BoolResult) {
    let param: Param = [
      "username": "appDevTest",
      "password": "tti2020"
    ]
    service.postFetchToken(param: param) { [weak self] status in
      guard
        status else {
        completion(false)
        return
      }
      completion(true)
    }
  }

  func getList(completion: @escaping BoolResult) {
    let param: Param = [
      "searchCriteria[sortOrders][0][direction]": "ASC",
      "searchCriteria[sortOrders][0][field]": "code",
      "fields": "items[sku,name]"
    ]
    service.postFetchList(param: param) { [weak self] response, status in
      guard let self = self,
            status else {
        completion(false)
        return
      }
      self.items.accept(response?.items ?? [])
      completion(true)
    }
  }

  func sortAscending() {
    let products = Array(items.value)
    items.accept(products.sorted { $0.name < $1.name })
  }

  func sortDescending() {
    let products = Array(items.value)
    items.accept(products.sorted { $0.name > $1.name })
  }
}
