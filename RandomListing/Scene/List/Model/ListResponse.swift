//
//  ListResponse.swift
//  RandomListing
//
//  Created by Louise Nicolas Namoc on 6/18/21.
//

import Foundation

// MARK: - ListResponse

struct ListResponse: Codable {
  let items: [Item]
}

// MARK: - Item

struct Item: Codable {
  let sku, name: String
}
