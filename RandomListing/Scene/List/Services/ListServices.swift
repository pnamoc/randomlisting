//
//  ListServices.swift
//  RandomListing
//
//  Created by Louise Nicolas Namoc on 6/18/21.
//

import Alamofire
import Foundation
import SwiftyUserDefaults

protocol ListServicesProtocol {
  func postFetchToken(
    param: Parameters,
    completion: @escaping SingleResult<Bool>
  )
  func postFetchList(
    param: Parameters,
    completion: @escaping DoubleResult<ListResponse?, Bool>
  )
}

final class ListServices: BaseService, ListServicesProtocol {
  func postFetchToken(
    param: Parameters,
    completion: @escaping SingleResult<Bool>
  ) {
    consumeToken(
      usingUrl: tokenEndPoint,
      withVerb: .POST,
      andParameters: param
    ) { _, error in
      guard error == nil else {
        completion(false)
        return
      }
      completion(true)
    }
  }

  func postFetchList(
    param: Parameters,
    completion: @escaping DoubleResult<ListResponse?, Bool>
  ) {
    consumeAPI(
      ListResponse.self,
      usingUrl: listProductEndPoint,
      withVerb: .GET,
      andParameters: param
    ) { response, error in
      guard error == nil else {
        completion(nil, false)
        return
      }
      completion(response, true)
    }
  }
}
