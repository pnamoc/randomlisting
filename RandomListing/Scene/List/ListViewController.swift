//
//  ListViewController.swift
//  RandomListing
//
//  Created by Louise Nicolas Namoc on 6/18/21.
//

import NSObject_Rx
import RxCocoa
import RxSwift
import SnapKit
import SwiftyUserDefaults
import UIKit

class ListViewController: UIViewController {
  // TableView Creation
  private lazy var tableView: UITableView = {
    let tableView = UITableView()
    tableView.register(
      ListCell.self,
      forCellReuseIdentifier: ListCell.identifier
    )
    tableView.tableFooterView = UIView()
    tableView.separatorStyle = .none
    return tableView
  }()

  // ViewModel Initialization
  private var viewModel = ListViewModel(service: ListServices())

  // Reload Button Creation
  private let reloadButton: UIButton = {
    let button = UIButton(type: .custom)
    button.setImage(#imageLiteral(resourceName: "reload"), for: .normal)
    button.contentEdgeInsets = UIEdgeInsets(top: 10, left: 10, bottom: 10, right: 10)
    button.backgroundColor = .gray
    button.addTarget(
      self,
      action: #selector(reloadTapped(sender:)), for: .touchUpInside
    )
    return button
  }()

  // Sort Button Creation
  private let sortButton: UIButton = {
    let button = UIButton(type: .custom)
    button.setImage(#imageLiteral(resourceName: "descending"), for: .normal)
    button.contentEdgeInsets = UIEdgeInsets(top: 10, left: 10, bottom: 10, right: 10)
    button.backgroundColor = .gray
    button.addTarget(
      self,
      action: #selector(sortTapped(sender:)), for: .touchUpInside
    )
    return button
  }()

  override func viewDidLoad() {
    super.viewDidLoad()
    setupNavigation()
    bindTableView()
    fetchToken()
  }

  override func viewWillAppear(_ animated: Bool) {
    super.viewWillAppear(animated)
    tableView.reloadData()
  }

  override func loadView() {
    super.loadView()
    setupTableView()
    setupButtons()
  }
}

private extension ListViewController {
  func setupNavigation() {
    navigationItem.title = "List"
  }

  // Setup Tableview
  func setupTableView() {
    view.addSubview(tableView)
    tableView.translatesAutoresizingMaskIntoConstraints = false
    tableView.snp.makeConstraints { m in
      m.leading.trailing.top.bottom.equalToSuperview()
    }
  }

  // Setup Bind Tableview
  func bindTableView() {
    tableView.register(
      ListCell.self,
      forCellReuseIdentifier: ListCell.identifier
    )

    viewModel.items.asObservable().bind(to: tableView.rx.items(
      cellIdentifier: ListCell.identifier,
      cellType: ListCell.self
    )) { _, item, cell in
      cell.configureCell(item: item)
    }.disposed(by: rx.disposeBag)

    tableView.rx.modelSelected(Item.self)
      .subscribe(onNext: { [weak self] item in
        guard let self = self else { return }
        self.goToDetails(item: item)
      }).disposed(by: rx.disposeBag)
  }

  // Setup Buttons
  func setupButtons() {
    let stackView = UIStackView()
    stackView.axis = .vertical
    stackView.distribution = .fill
    stackView.alignment = .fill
    stackView.spacing = 5.0

    reloadButton.layer.cornerRadius = 5
    sortButton.layer.cornerRadius = 5
    reloadButton.updateTint(color: .white)
    sortButton.updateTint(color: .white)

    stackView.addArrangedSubview(reloadButton)
    stackView.addArrangedSubview(sortButton)

    view.addSubview(stackView)
    stackView.snp.makeConstraints { m in
      m.bottom.equalTo(view.safeAreaLayoutGuide.snp.bottom)
      m.trailing.equalToSuperview().offset(-10)
    }
  }

  @objc
  func reloadTapped(sender: UIButton) {
    if let validToken = Defaults[\.token], !validToken.isEmpty {
      fetchProducts()
    }
  }

  @objc
  func sortTapped(sender: UIButton) {
    sender.isSelected = !sender.isSelected
    sender.isSelected ? viewModel.sortAscending() : viewModel.sortDescending()
    let image = UIImage(named: sender.isSelected ? "ascending" : "descending")
    sortButton.setImage(image, for: .normal)
    sortButton.updateTint(color: .white)
  }

  func fetchToken() {
    ProgressHud.shared.show()
    viewModel.getToken(completion: handleTokenSuccess())
  }

  func fetchProducts() {
    DispatchQueue.main.async { [weak self] in
      guard let self = self else { return }
      ProgressHud.shared.show()
      self.viewModel.getList(completion: self.handleProductListSuccess())
    }
  }

  func goToDetails(item: Item) {
    let vc = DetailViewController()
    vc.bind(item: item)
    navigationController?.pushViewController(vc, animated: true)
  }
}

// API Call Back Methods
extension ListViewController {
  func handleTokenSuccess() -> BoolResult {
    return { [weak self] status in
      guard let self = self, status else { return }
      ProgressHud.shared.dismiss()
      self.fetchProducts()
    }
  }

  func handleProductListSuccess() -> BoolResult {
    return { _ in
      ProgressHud.shared.dismiss()
    }
  }
}
