//
//  DetailViewModel.swift
//  RandomListing
//
//  Created by Louise Nicolas Namoc on 6/21/21.
//

import Foundation
import RxCocoa
import RxSwift

class DetailViewModel {
  private var service: DetailServicesProtocol

  var item: Item?
  var detail: DetailResponse?
  init(service: DetailServicesProtocol) {
    self.service = service
  }

  func getDetails(completion: @escaping BoolResult) {
    service.postFetchProductDetails(sku: item?.sku ?? "") { [weak self] response, status in
      guard let self = self,
            status else {
        completion(false)
        return
      }
      self.detail = response
      completion(true)
    }
  }
}
