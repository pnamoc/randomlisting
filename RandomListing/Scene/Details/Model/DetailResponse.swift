//
//  DetailResponse.swift
//  RandomListing
//
//  Created by Louise Nicolas Namoc on 6/21/21.
//

import Foundation

// MARK: - DetailResponse

struct DetailResponse: Codable {
  let id: Int
  let sku, name: String
  let attributeSetID, price, status, visibility: Int
  let typeID, createdAt, updatedAt: String
  let weight: Int
  let extensionAttributes: ExtensionAttributes
  let mediaGalleryEntries: [MediaGalleryEntry]
  let customAttributes: [CustomAttribute]

  enum CodingKeys: String, CodingKey {
    case id, sku, name
    case attributeSetID = "attribute_set_id"
    case price, status, visibility
    case typeID = "type_id"
    case createdAt = "created_at"
    case updatedAt = "updated_at"
    case weight
    case extensionAttributes = "extension_attributes"
    case mediaGalleryEntries = "media_gallery_entries"
    case customAttributes = "custom_attributes"
  }
}

// MARK: - CustomAttribute

struct CustomAttribute: Codable {
  let attributeCode: String?
  var value: [String]?

  init(from decoder: Decoder) throws {
    let container = try decoder.container(keyedBy: CodingKeys.self)
    attributeCode = try? container.decode(String.self, forKey: .attributeCode)

    if let valueStr = try? container.decode(String.self, forKey: .value) {
      value?.append(valueStr)
    } else {
      value? = try container.decode([String].self, forKey: .value)
    }
  }

  enum CodingKeys: String, CodingKey {
    case attributeCode = "attribute_code"
    case value
  }
}

// MARK: - ExtensionAttributes

struct ExtensionAttributes: Codable {
  let websiteIDS: [Int]
  let categoryLinks: [CategoryLink]?
  let stockItem: StockItem?

  enum CodingKeys: String, CodingKey {
    case websiteIDS = "website_ids"
    case categoryLinks = "category_links"
    case stockItem = "stock_item"
  }
}

// MARK: - CategoryLink

struct CategoryLink: Codable {
  let position: Int
  let categoryID: String

  enum CodingKeys: String, CodingKey {
    case position
    case categoryID = "category_id"
  }
}

// MARK: - StockItem

struct StockItem: Codable {
  let itemID, productID, stockID, qty: Int
  let isInStock, isQtyDecimal, showDefaultNotificationMessage, useConfigMinQty: Bool
  let minQty, useConfigMinSaleQty, minSaleQty: Int
  let useConfigMaxSaleQty: Bool
  let maxSaleQty: Int
  let useConfigBackorders: Bool
  let backorders: Int
  let useConfigNotifyStockQty: Bool
  let notifyStockQty: Int
  let useConfigQtyIncrements: Bool
  let qtyIncrements: Int
  let useConfigEnableQtyInc, enableQtyIncrements, useConfigManageStock, manageStock: Bool
  let lowStockDate: String
  let isDecimalDivided: Bool
  let stockStatusChangedAuto: Int

  enum CodingKeys: String, CodingKey {
    case itemID = "item_id"
    case productID = "product_id"
    case stockID = "stock_id"
    case qty
    case isInStock = "is_in_stock"
    case isQtyDecimal = "is_qty_decimal"
    case showDefaultNotificationMessage = "show_default_notification_message"
    case useConfigMinQty = "use_config_min_qty"
    case minQty = "min_qty"
    case useConfigMinSaleQty = "use_config_min_sale_qty"
    case minSaleQty = "min_sale_qty"
    case useConfigMaxSaleQty = "use_config_max_sale_qty"
    case maxSaleQty = "max_sale_qty"
    case useConfigBackorders = "use_config_backorders"
    case backorders
    case useConfigNotifyStockQty = "use_config_notify_stock_qty"
    case notifyStockQty = "notify_stock_qty"
    case useConfigQtyIncrements = "use_config_qty_increments"
    case qtyIncrements = "qty_increments"
    case useConfigEnableQtyInc = "use_config_enable_qty_inc"
    case enableQtyIncrements = "enable_qty_increments"
    case useConfigManageStock = "use_config_manage_stock"
    case manageStock = "manage_stock"
    case lowStockDate = "low_stock_date"
    case isDecimalDivided = "is_decimal_divided"
    case stockStatusChangedAuto = "stock_status_changed_auto"
  }
}

// MARK: - MediaGalleryEntry

struct MediaGalleryEntry: Codable {
  let id: Int?
  let mediaType, label: String?
  let position: Int?
  let disabled: Bool?
  let types: [String]?
  let file: String?

  enum CodingKeys: String, CodingKey {
    case id
    case mediaType = "media_type"
    case label, position, disabled, types, file
  }
}
