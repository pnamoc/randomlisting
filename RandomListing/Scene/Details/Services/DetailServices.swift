//
//  ListServices.swift
//  RandomListing
//
//  Created by Louise Nicolas Namoc on 6/18/21.
//

import Alamofire
import Foundation
import SwiftyUserDefaults

protocol DetailServicesProtocol {
  func postFetchProductDetails(
    sku: String,
    completion: @escaping DoubleResult<DetailResponse?, Bool>
  )
}

final class DetailServices: BaseService, DetailServicesProtocol {
  func postFetchProductDetails(
    sku: String,
    completion: @escaping DoubleResult<DetailResponse?, Bool>
  ) {
    let detailUrl = "\(listProductEndPoint)/\(sku)"
    consumeAPI(
      DetailResponse.self,
      usingUrl: detailUrl,
      withVerb: .GET
    ) { response, error in
      guard error == nil else {
        completion(nil, false)
        return
      }
      completion(response, true)
    }
  }
}
