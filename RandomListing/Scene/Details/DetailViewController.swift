//
//  UIViewcontroller.swift
//  RandomListing
//
//  Created by Louise Nicolas Namoc on 6/21/21.
//

import RealmSwift
import UIKit

class DetailViewController: UIViewController {
  var realmNotification: NotificationToken?

  // ScrollView Creation
  private let scrollView: UIScrollView = {
    let scroll = UIScrollView()
    scroll.backgroundColor = .white
    return scroll
  }()

  // ContentView Creation
  private let contentView: UIView = {
    let view = UIView()
    view.backgroundColor = .white
    return view
  }()

  // Name Label Creation
  private let nameLabel: UILabel = {
    let label = UILabel()
    label.textColor = .black
    label.numberOfLines = 0
    return label
  }()

  // Price Label Creation
  private let priceLabel: UILabel = {
    let label = UILabel()
    label.textColor = .black
    label.numberOfLines = 0

    return label
  }()

  // Weight Label Creation
  private let weightLabel: UILabel = {
    let label = UILabel()
    label.textColor = .black
    label.numberOfLines = 0
    return label
  }()

  // TypeLabel Creation
  private let typeLabel: UILabel = {
    let label = UILabel()
    label.textColor = .black
    label.numberOfLines = 0
    return label
  }()

  private var viewModel = DetailViewModel(service: DetailServices())

  func bind(item: Item) {
    viewModel.item = item
  }

  override func viewDidLoad() {
    super.viewDidLoad()
    setupNavigation()
    getProductDetails()
  }

  override func loadView() {
    super.loadView()
    view.backgroundColor = .white

    setupScrollView()
  }
}

private extension DetailViewController {
  func setupNavigation() {
    navigationItem.title = viewModel.item?.sku
    var image = UIImage()
    let realm = RealmService.shared.realm
    if realm.objects(FavoritesModel.self).filter("sku = %@", viewModel.item?.sku ?? "").first != nil {
      image = UIImage(named: "star")!
    } else {
      image = (UIImage(named: "blackStar")?.maskWithColor(color: .gray))!
    }
    addRightBarButtonItem(
      with: image,
      target: self,
      selector: #selector(rightBarButtonItemTapped)
    )
  }

  @objc
  func rightBarButtonItemTapped() {
    let favorites = FavoritesModel()
    favorites.name = viewModel.item?.name ?? ""
    favorites.sku = viewModel.item?.sku ?? ""

    var image = UIImage()

    let realm = RealmService.shared.realm
    if let result = realm.objects(FavoritesModel.self).filter("sku = %@", viewModel.item?.sku ?? "").first {
      RealmService.shared.delete(result)
      image = (UIImage(named: "blackStar")?.maskWithColor(color: .gray))!

    } else {
      RealmService.shared.create(favorites)
      image = UIImage(named: "star")!
    }
    addRightBarButtonItem(
      with: image,
      target: self,
      selector: #selector(rightBarButtonItemTapped)
    )
  }

  func getProductDetails() {
    ProgressHud.shared.show()
    viewModel.getDetails(completion: handleDetailSuccess())
  }

  func setupScrollView() {
    scrollView.translatesAutoresizingMaskIntoConstraints = false
    contentView.translatesAutoresizingMaskIntoConstraints = false

    view.addSubview(scrollView)
    scrollView.snp.makeConstraints { m in
      m.top.bottom.equalToSuperview()
      m.leading.trailing.equalToSuperview()
      m.width.equalTo(view)
    }
    scrollView.addSubview(contentView)
    contentView.snp.makeConstraints { m in
      m.top.bottom.equalTo(self.scrollView)
      m.left.right.equalTo(self.view)
      m.width.equalTo(self.scrollView)
    }

    contentView.addSubview(nameLabel)
    nameLabel.snp.makeConstraints { m in
      m.top.equalToSuperview().offset(20)
      m.leadingMargin.equalTo(10)
      m.trailingMargin.equalTo(-10)
    }

    contentView.addSubview(priceLabel)
    priceLabel.snp.makeConstraints { m in
      m.top.equalTo(nameLabel.snp.bottom)
      m.leadingMargin.equalTo(10)
      m.trailingMargin.equalTo(-10)
    }

    contentView.addSubview(weightLabel)
    weightLabel.snp.makeConstraints { m in
      m.top.equalTo(priceLabel.snp.bottom)
      m.leadingMargin.equalTo(10)
      m.trailingMargin.equalTo(-10)
    }

    contentView.addSubview(typeLabel)
    typeLabel.snp.makeConstraints { m in
      m.top.equalTo(weightLabel.snp.bottom)
      m.leadingMargin.equalTo(10)
      m.trailingMargin.equalTo(-10)
      m.bottom.equalTo(contentView).offset(-20)
    }
  }
}

extension DetailViewController {
}

extension DetailViewController {
  func handleDetailSuccess() -> BoolResult {
    return { [weak self] _ in
      guard let self = self else { return }
      ProgressHud.shared.dismiss()
      self.populateData()
    }
  }

  func populateData() {
    nameLabel.text = "Name: \(viewModel.detail?.name ?? "N/A")"
    priceLabel.text = "Price: \(viewModel.detail?.price ?? 0)"
    weightLabel.text = "Weight: \(viewModel.detail?.weight ?? 0)"
    typeLabel.text = "Type: \(viewModel.detail?.typeID ?? "N/A")"
  }
}
